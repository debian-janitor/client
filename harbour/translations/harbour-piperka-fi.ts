<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fi" sourcelanguage="en">
<context>
    <name>AllReadPage</name>
    <message>
        <source>All caught up!</source>
        <translation>Kaikki luettu!</translation>
    </message>
</context>
<context>
    <name>BrowseItem</name>
    <message>
        <source>Subscribe</source>
        <translation>Tilaa</translation>
    </message>
    <message>
        <source>Unsubscribe</source>
        <translation>Poista tilaus</translation>
    </message>
    <message>
        <source>Unsubscribing</source>
        <translation>Tilaus poistetaan</translation>
    </message>
    <message>
        <source>View entry on Piperka</source>
        <translation>Katso sarjakuvan tiedot Piperkassa</translation>
    </message>
</context>
<context>
    <name>BrowsePage</name>
    <message>
        <source>Options</source>
        <translation>Asetukset</translation>
    </message>
    <message>
        <source>Browse comics</source>
        <translation>Selaa sarjakuvalistaa</translation>
    </message>
    <message>
        <source>Sort type</source>
        <translation>Lajittelutapa</translation>
    </message>
    <message>
        <source>Alphabetical</source>
        <translation>Aakkosellinen</translation>
    </message>
    <message>
        <source>Most popular</source>
        <translation>Suosituin ensin</translation>
    </message>
    <message>
        <source>Date added</source>
        <translation>Viimeksi lisätty ensin</translation>
    </message>
    <message>
        <source>Most recently updated</source>
        <translation>Viimeksi päivitetty</translation>
    </message>
    <message>
        <source>Bookmark first page</source>
        <translation>Aseta kirjanmerkki ensimmäiselle sivulle</translation>
    </message>
    <message>
        <source>Show only subscribed comics</source>
        <translation>Näytä vain tilatut sarjakuvat</translation>
    </message>
</context>
<context>
    <name>CoverPage</name>
    <message>
        <source>All caught up!</source>
        <translation>Kaikki luettu!</translation>
    </message>
</context>
<context>
    <name>ForceLogout</name>
    <message>
        <source>The server didn&apos;t recognize your user session. You have been logged out. Please try logging in again.</source>
        <translation>Palvelin ei tunnistanut käyttäjäsessiotasi. Sinut on kirjattu ulos.</translation>
    </message>
</context>
<context>
    <name>LoginPage</name>
    <message>
        <source>Login</source>
        <translation>Kirjaudu</translation>
    </message>
    <message>
        <source>User name</source>
        <translation>Käyttäjänimi</translation>
    </message>
    <message>
        <source>Password</source>
        <translation>Salasana</translation>
    </message>
    <message>
        <source>Remember me</source>
        <translation>Muista minut</translation>
    </message>
    <message>
        <source>Import bookmarks</source>
        <translation>Tuo kirjanmerkit</translation>
    </message>
    <message>
        <source>Importing bookmarks will overwrite the ones on server. Not importing will discard local bookmarks.</source>
        <translation>Kirjanmerkkien tuominen ylikirjoittaa palvelimelle tallennetut kirjanmerkit. Tuomatta jättäminen hylkää kirjanmerkit.</translation>
    </message>
</context>
<context>
    <name>MainPage</name>
    <message>
        <source>Create account</source>
        <translation>Luo käyttäjätunnus</translation>
    </message>
    <message>
        <source>Login</source>
        <translation>Kirjaudu</translation>
    </message>
    <message>
        <source>Logout</source>
        <translation>Kirjaudu ulos</translation>
    </message>
    <message>
        <source>Logging out</source>
        <translation>Uloskirjaudutaan</translation>
    </message>
    <message>
        <source>Synchronize now</source>
        <translation>Synkronoi nyt</translation>
    </message>
    <message>
        <source>Browse comics</source>
        <translation>Selaa sarjakuvalistaa</translation>
    </message>
    <message>
        <source>Updates</source>
        <translation>Päivitykset</translation>
    </message>
    <message>
        <source>Quick read</source>
        <translation>Pikaluku</translation>
    </message>
    <message>
        <source>You have no unread comics. Wait for updates or subscribe to more comics.</source>
        <translation>Sinulla ei ole lukemattomia sarjakuvia. Odota päivityksiä tai lisää valintoihisi uusia sarjakuvia.</translation>
    </message>
    <message>
        <source>Select comics to read from the browse comics page.</source>
        <translation>Valitse luettavia sarjakuvia sarjakuvalistasta.</translation>
    </message>
    <message>
        <source>Last %L1 scheduled syncs failed. The client will retry hourly.</source>
        <translation>Viimeiset %L1 ajastettua päivitystä epäonnistuivat. Ohjelma yrittää uudestaan tunneittain.</translation>
    </message>
    <message>
        <source>The last scheduled sync failed. The client will retry hourly.</source>
        <translation>Viimeisin ajastettu päivitys epäonnistui. Ohjelma yrittää uudestaan tunneittain.</translation>
    </message>
    <message>
        <source>Log in or create account and subscribe to at least 5 comics to get recommendations.</source>
        <translation>Suosituksia nähdäksesi kirjaudu tai luo tunnus ja tilaa vähintään 5 sarjakuvaa.</translation>
    </message>
    <message>
        <source>Recommendations</source>
        <translation>Suositukset</translation>
    </message>
</context>
<context>
    <name>NetworkErrorPage</name>
    <message>
        <source>Detail</source>
        <translation>Yksityiskohdat</translation>
    </message>
    <message>
        <source>Network failure</source>
        <translation>Verkkovirhe</translation>
    </message>
</context>
<context>
    <name>NewAccountPage</name>
    <message>
        <source>Create account</source>
        <translation>Luo tunnus</translation>
    </message>
    <message>
        <source>User name</source>
        <translation>Käyttäjänimi</translation>
    </message>
    <message>
        <source>email (optional)</source>
        <translation>email (vapaaehtoinen)</translation>
    </message>
    <message>
        <source>email</source>
        <translation>email</translation>
    </message>
    <message>
        <source>Password</source>
        <translation>Salasana</translation>
    </message>
    <message>
        <source>Retype password</source>
        <translation>Salasana uudestaan</translation>
    </message>
    <message>
        <source>Account name reserved. Please try again.</source>
        <translation>Käyttäjänimi on varattu. Yritä uudestaan.</translation>
    </message>
    <message>
        <source>Remember me</source>
        <translation>Muista minut</translation>
    </message>
</context>
<context>
    <name>PageDetailPage</name>
    <message>
        <source>Details</source>
        <translation>Yksityiskohdat</translation>
    </message>
    <message>
        <source>Copy to clipboard</source>
        <translation>Kopioi leikepöydälle</translation>
    </message>
    <message>
        <source>Homepage</source>
        <translation>Kotisivu</translation>
    </message>
    <message>
        <source>Jump to...</source>
        <translation>Hyppää...</translation>
    </message>
    <message>
        <source>Currently viewing</source>
        <translation>Nyt avattu sivu</translation>
    </message>
    <message>
        <source>Subscribed</source>
        <translation>Tilattu sivu</translation>
    </message>
    <message>
        <source>Current</source>
        <translation>Nykyinen</translation>
    </message>
    <message>
        <source>Set bookmark</source>
        <translation>Aseta kirjamerkki</translation>
    </message>
    <message>
        <source>Subscription controls</source>
        <translation>Tilauksenhallinta</translation>
    </message>
    <message>
        <source>Move bookmark on navigation</source>
        <translation>Siirrä kirjanmerkkiä navigoidessa</translation>
    </message>
    <message>
        <source>Newest page</source>
        <translation>Uusin sivu</translation>
    </message>
    <message>
        <source>Use the following link to open in external browser</source>
        <translation>Käytä seuraavaa linkkiä avataksesi sivun erillisessä selaimessa</translation>
    </message>
</context>
<context>
    <name>RecommendPage</name>
    <message>
        <source>For some reason, no recommendations were received. Try again later.</source>
        <translation>Jostain syystä palvelimelta ei saatu suosituksia. Yritä uudestaan myöhemmin.</translation>
    </message>
    <message>
        <source>Recommendations</source>
        <translation>Suositukset</translation>
    </message>
</context>
<context>
    <name>UpdatesPage</name>
    <message>
        <source>Synchronize now</source>
        <translation>Synkronoi nyt</translation>
    </message>
    <message>
        <source>Updates</source>
        <translation>Päivittyneet sarjakuvat</translation>
    </message>
    <message>
        <source>Sort type</source>
        <translation>Lajittelutapa</translation>
    </message>
    <message>
        <source>Least new pages first</source>
        <translation>Vähiten päivittyneitä sivuja ensin</translation>
    </message>
    <message>
        <source>Most recently updated</source>
        <translation>Viimeksi päivitetty</translation>
    </message>
    <message>
        <source>Alphabetical</source>
        <translation>Aakkosellinen</translation>
    </message>
    <message>
        <source>Offset back by one</source>
        <translation>Avaa luettu sivu ensin</translation>
    </message>
</context>
</TS>
