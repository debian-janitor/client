<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr">
<context>
    <name>AllReadPage</name>
    <message>
        <source>All caught up!</source>
        <translation>Tout à jour!</translation>
    </message>
</context>
<context>
    <name>BrowseItem</name>
    <message>
        <source>Subscribe</source>
        <translation>S&apos;abonner</translation>
    </message>
    <message>
        <source>Unsubscribe</source>
        <translation>Se désabonner</translation>
    </message>
    <message>
        <source>Unsubscribing</source>
        <translation>Désabonnement</translation>
    </message>
    <message>
        <source>View entry on Piperka</source>
        <translation>Afficher l&apos;entrée sur Piperka</translation>
    </message>
</context>
<context>
    <name>BrowsePage</name>
    <message>
        <source>Options</source>
        <translation>Paramètres</translation>
    </message>
    <message>
        <source>Browse comics</source>
        <translation>Explorer les comics</translation>
    </message>
    <message>
        <source>Sort type</source>
        <translation>Mode de tri</translation>
    </message>
    <message>
        <source>Alphabetical</source>
        <translation>Alphabétique</translation>
    </message>
    <message>
        <source>Most popular</source>
        <translation>Plus populaire</translation>
    </message>
    <message>
        <source>Date added</source>
        <translation>Date d&apos;ajout</translation>
    </message>
    <message>
        <source>Most recently updated</source>
        <translation>Mises à jour récentes</translation>
    </message>
    <message>
        <source>Bookmark first page</source>
        <translation>Ajouter la première page aux favoris</translation>
    </message>
    <message>
        <source>Show only subscribed comics</source>
        <translation>Seulement afficher les comics souscrits</translation>
    </message>
</context>
<context>
    <name>CoverPage</name>
    <message>
        <source>All caught up!</source>
        <translation>Tout à jour!</translation>
    </message>
</context>
<context>
    <name>ForceLogout</name>
    <message>
        <source>The server didn&apos;t recognize your user session. You have been logged out. Please try logging in again.</source>
        <translation>Le serveur n&apos;a pas reconnu votre session ; vous avez été déconnecté. Merci de vous reconnecter.</translation>
    </message>
</context>
<context>
    <name>LoginPage</name>
    <message>
        <source>Login</source>
        <translation>Connexion</translation>
    </message>
    <message>
        <source>User name</source>
        <translation>Nom d&apos;utilisateur</translation>
    </message>
    <message>
        <source>Password</source>
        <translation>Mot de passe</translation>
    </message>
    <message>
        <source>Remember me</source>
        <translation>Se souvenir de moi</translation>
    </message>
    <message>
        <source>Import bookmarks</source>
        <translation>Importer les favoris</translation>
    </message>
    <message>
        <source>Importing bookmarks will overwrite the ones on server. Not importing will discard local bookmarks.</source>
        <translation>Importer les favoris va importer ceux présents sur le serveur ; ne pas les importer va abandonner ceux en local.</translation>
    </message>
</context>
<context>
    <name>MainPage</name>
    <message>
        <source>Create account</source>
        <translation>Créer un compte</translation>
    </message>
    <message>
        <source>Login</source>
        <translation>Se connecter</translation>
    </message>
    <message>
        <source>Logout</source>
        <translation>Se déconnecter</translation>
    </message>
    <message>
        <source>Logging out</source>
        <translation>Déconnexion en cours</translation>
    </message>
    <message>
        <source>Synchronize now</source>
        <translation>Synchroniser</translation>
    </message>
    <message>
        <source>Browse comics</source>
        <translation>Parcourir les comics</translation>
    </message>
    <message>
        <source>Updates</source>
        <translation>Mises à jour</translation>
    </message>
    <message>
        <source>Quick read</source>
        <translation>Lecture rapide</translation>
    </message>
    <message>
        <source>You have no unread comics. Wait for updates or subscribe to more comics.</source>
        <translation>Vous n&apos;avez pas de comic non lu. Attendez des mises à jour ou souscrivez à d&apos;autres comics.</translation>
    </message>
    <message>
        <source>Select comics to read from the browse comics page.</source>
        <translation>Sélectionner des comics à lire depuis la page &quot;Parcourir les comics&quot;.</translation>
    </message>
    <message>
        <source>Last %L1 scheduled syncs failed. The client will retry hourly.</source>
        <translation>Les %L1 dernières synchronisations automatiques ont échoué. Le client réessaiera chaque heure.</translation>
    </message>
    <message>
        <source>The last scheduled sync failed. The client will retry hourly.</source>
        <translation>La dernière synchronisation automatique a échoué. Le client réessaiera chaque heure.</translation>
    </message>
    <message>
        <source>Recommendations</source>
        <translation>Recommandations</translation>
    </message>
    <message>
        <source>Log in or create account and subscribe to at least 5 comics to get recommendations.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>NetworkErrorPage</name>
    <message>
        <source>Detail</source>
        <translation>Détails</translation>
    </message>
    <message>
        <source>Network failure</source>
        <translation>Erreur réseau</translation>
    </message>
</context>
<context>
    <name>NewAccountPage</name>
    <message>
        <source>Create account</source>
        <translation>Créer un compte</translation>
    </message>
    <message>
        <source>User name</source>
        <translation>Nom d&apos;utilisateur</translation>
    </message>
    <message>
        <source>email (optional)</source>
        <translation>E-mail (optionel)</translation>
    </message>
    <message>
        <source>email</source>
        <translation>E-mail</translation>
    </message>
    <message>
        <source>Password</source>
        <translation>Mot de passe</translation>
    </message>
    <message>
        <source>Retype password</source>
        <translation>Retaper le mot de passe</translation>
    </message>
    <message>
        <source>Account name reserved. Please try again.</source>
        <translation>Nom de compte déjà utilisé, merci de réessayer.</translation>
    </message>
    <message>
        <source>Remember me</source>
        <translation>Se souvenir de moi</translation>
    </message>
</context>
<context>
    <name>PageDetailPage</name>
    <message>
        <source>Details</source>
        <translation>Détails</translation>
    </message>
    <message>
        <source>Copy to clipboard</source>
        <translation>Copier dans le presse-papier</translation>
    </message>
    <message>
        <source>Homepage</source>
        <translation>Accueil</translation>
    </message>
    <message>
        <source>Jump to...</source>
        <translation>Aller à...</translation>
    </message>
    <message>
        <source>Currently viewing</source>
        <translation>Actuellement lus</translation>
    </message>
    <message>
        <source>Subscribed</source>
        <translation>Souscrits</translation>
    </message>
    <message>
        <source>Current</source>
        <translation>Actuel</translation>
    </message>
    <message>
        <source>Set bookmark</source>
        <translation>Ajouter en favori</translation>
    </message>
    <message>
        <source>Subscription controls</source>
        <translation>Contrôles de souscriptions</translation>
    </message>
    <message>
        <source>Move bookmark on navigation</source>
        <translation>Déplacer le favori en navigation</translation>
    </message>
    <message>
        <source>Newest page</source>
        <translation>Plus récente page</translation>
    </message>
    <message>
        <source>Use the following link to open in external browser</source>
        <translation>Utilisez ce lien pour ouvrir la page dans un navigateur externe</translation>
    </message>
</context>
<context>
    <name>RecommendPage</name>
    <message>
        <source>For some reason, no recommendations were received. Try again later.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Recommendations</source>
        <translation>Recommandations</translation>
    </message>
</context>
<context>
    <name>UpdatesPage</name>
    <message>
        <source>Synchronize now</source>
        <translation>Synchronisez maintenant</translation>
    </message>
    <message>
        <source>Updates</source>
        <translation>Mises à jour</translation>
    </message>
    <message>
        <source>Sort type</source>
        <translation>Type de tri</translation>
    </message>
    <message>
        <source>Least new pages first</source>
        <translation>Le moins de mises à jour en premier</translation>
    </message>
    <message>
        <source>Most recently updated</source>
        <translation>Mis à jour le plus récemment</translation>
    </message>
    <message>
        <source>Alphabetical</source>
        <translation>Alphabétique</translation>
    </message>
    <message>
        <source>Offset back by one</source>
        <translation>jkkDécaler par un</translation>
    </message>
</context>
</TS>
