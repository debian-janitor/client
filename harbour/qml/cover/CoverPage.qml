/**************************************************************************
** Piperka Client
** Copyright (C) 2019  Kari Pahula
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License along
** with this program; if not, write to the Free Software Foundation, Inc.,
** 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
**************************************************************************/

import QtQuick 2.0
import Sailfish.Silica 1.0

CoverBackground {
    id: cover
    Connections {
        target: cover
        /* Apparenlty the cover gets these on screen unlock.
         * Use it to trigger sync if it's time for it.
         */
        onStatusChanged: {
            if (cover.status === 1)
                user.unlockSync();
        }
    }

    Connections {
        target: user
        onSynchronize: {
            coverUpdates.scrollToTop();
        }
    }

    BusyIndicator {
        id: busy
        size: BusyIndicatorSize.Large
        anchors.centerIn: parent
        running: user.loading
    }

    Item {
        anchors.fill: parent
        visible: updatesModel.noUnread && !user.loading
        SectionHeader {
            id: header
            text: "Piperka"
        }

        Item {
            anchors {
                top: header.bottom
                left: parent.left
                right: parent.right
                bottom: parent.bottom
            }
            Label {
                anchors.centerIn: parent
                text: qsTr("All caught up!")
                color: Theme.highlightColor
                wrapMode: Text.WordWrap
            }
        }
    }

    SilicaListView {
        id: coverUpdates
        anchors.fill: parent
        visible: !updatesModel.noUnread
        model: updatesModel

        header: SectionHeader {
            text: "Piperka"
        }

        delegate: ListItem {
            id: delegate

            Label {
                x: Theme.horizontalPageMargin
                id: unreadCount
                width: Theme.itemSizeExtraSmall
                anchors.verticalCenter: parent.verticalCenter
                text: unread_count
                font.pixelSize: Theme.fontSizeExtraSmall
            }

            Label {
                anchors {
                    verticalCenter: parent.verticalCenter
                    left: unreadCount.right
                }
                text: title
                font.pixelSize: Theme.fontSizeExtraSmall
                truncationMode: TruncationMode.Fade
            }
            height: unreadCount.height
        }
    }

    CoverActionList {
        id: coverAction
        enabled: !updatesModel.noUnread

        CoverAction {
            iconSource: "image://theme/icon-cover-play"
            onTriggered: {
                appWindow.pageStack.clear();
                appWindow.pageStack.push(Qt.resolvedUrl("../pages/MainPage.qml"), {}, PageStackAction.Immediate)
                appWindow.pageStack.push(Qt.resolvedUrl("../pages/UpdatesPage.qml"), {}, PageStackAction.Immediate)
                pageModel.loadComic(updatesModel.firstCid());
                pageModel.autoBookmark = true;
                pageModel.autoSwitch = true;
                appWindow.pageStack.push(Qt.resolvedUrl("../pages/ReaderPage.qml"), {}, PageStackAction.Immediate);
                appWindow.activate();
            }
        }
    }
}
