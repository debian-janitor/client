/**************************************************************************
** Piperka Client
** Copyright (C) 2019  Kari Pahula
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License along
** with this program; if not, write to the Free Software Foundation, Inc.,
** 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
**************************************************************************/

import QtQuick 2.0
import Sailfish.Silica 1.0
import "../components"

Page {
    id: browsePage

    BusyIndicator {
        size: BusyIndicatorSize.Large
        anchors.centerIn: parent
        running: user.loading
    }

    allowedOrientations: Orientation.All
    backNavigation: !panel.open

    Component.onCompleted: {
        browseModel.filterSubscribed = false;
        browseModel.sortType = 0;
        browseModel.setSearch("");
        pageModel.autoBookmark = false;
        pageModel.autoSwitch = false;
    }

    SilicaListView {
        id: browseList
        model: browseModel
        anchors.fill: parent
        currentIndex: -1

        VerticalScrollDecorator {
            flickable: browseList
        }

        PullDownMenu {
            MenuItem {
                id: browseOptionsItem
                text: qsTr("Options")
                onClicked: panel.open = true
            }
        }

        header: Column {
            width: parent.width

            PageHeader {
                id: header
                title: qsTr("Browse comics")
            }

            SearchField {
                width: parent.width
                onTextChanged: browseModel.setSearch(text)
            }
        }

        delegate: BrowseItem {
            bookmarkFirst: bookmarkFirstSwitch.checked
        }
    }

    DockedPanel {
        id: panel

        width: parent.width
        height: parent.height
        modal: true

        dock: Dock.Right

        Column {
            width: parent.width
            PageHeader {
                title: qsTr("Options")
            }

            ComboBox {
                id: sortType
                label: qsTr("Sort type")
                currentIndex: 0
                menu: ContextMenu {
                    MenuItem {
                        text: qsTr("Alphabetical")
                    }
                    MenuItem {
                        text: qsTr("Most popular")
                    }
                    MenuItem {
                        text: qsTr("Date added")
                    }
                    MenuItem {
                        text: qsTr("Most recently updated")
                    }
                    onClicked: {
                        browseModel.sortType = sortType.currentIndex
                    }
                }
            }

            TextSwitch {
                id: bookmarkFirstSwitch
                text: qsTr("Bookmark first page")
                anchors {
                    leftMargin: Theme.horizontalPageMargin
                    rightMargin: Theme.horizontalPageMargin
                }
		checked: true
            }

            TextSwitch {
                id: showOnlySubscribed
                objectName: "browseSubscribed"
                text: qsTr("Show only subscribed comics")
                anchors {
                    leftMargin: Theme.horizontalPageMargin
                    rightMargin: Theme.horizontalPageMargin
                }
                onClicked: browseModel.filterSubscribed = checked
            }
        }
     }
}
