/**************************************************************************
** Piperka Client
** Copyright (C) 2019  Kari Pahula
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License along
** with this program; if not, write to the Free Software Foundation, Inc.,
** 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
**************************************************************************/

import QtQuick 2.2
import Sailfish.Silica 1.0
import QtWebKit 3.0

Page {
    id: readerPage
    allowedOrientations: Orientation.All

    BusyIndicator {
        size: BusyIndicatorSize.Large
        anchors.centerIn: parent
        running: user.loading
    }

    SilicaWebView {
        id: webView

        anchors {
            top: parent.top
            left: parent.left
            right: parent.right
            bottom: naviRow.top
        }
        url: pageModel.cursorUri

        visible: !pageModel.allRead
    }

    /*
    LinkedLabel {
        anchors {
            top: parent.top
            left: parent.left
            right: parent.right
            bottom: naviRow.top
        }
        text: pageModel.cursorUri
        font.pixelSize: Theme.fontSizeExtraLarge
    }
    */

    Item {
        id: naviRow
        anchors {
            left: parent.left
            right: parent.right
            bottom: parent.bottom
        }
        height: Theme.itemSizeSmall

        IconButton {
            anchors.left: parent.left
            id: prev
            icon.source: "image://theme/icon-m-left"
            width: Theme.buttonWidthExtraSmall
            enabled: !pageModel.allread && pageModel.cursor.row > 0
            onClicked: pageModel.setCursor(pageModel.cursor.row-1)
        }

        Button {
            anchors.centerIn: parent
            id: options
            text: (1+pageModel.cursor.row) + "/" + (pageModel.rowCount-1)
            preferredWidth: Theme.buttonWidthMedium
            enabled: !pageModel.allRead
            onClicked: pageStack.push(Qt.resolvedUrl("PageDetailPage.qml"))
        }

        IconButton {
            anchors.right: parent.right
            id: next
            icon.source: pageModel.nextIsSwitch ?
                             "image://theme/icon-m-next" :
                             "image://theme/icon-m-right"
            width: Theme.buttonWidthExtraSmall
            enabled: pageModel.haveNext
            onClicked: {
                if (pageModel.nextIsSwitch) {
                    if (!pageModel.switchNext()) {
                        var depth = pageStack.depth
                        var i = 0
                        while (i++ < depth) {
                            pageStack.pop(null, PageStackAction.Immediate)
                        }
                        pageStack.push(Qt.resolvedUrl("AllReadPage.qml"), PageStackAction.Immediate)
                    }
                } else
                    pageModel.setCursorNext()
            }
        }
    }
}
