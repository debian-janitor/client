/**************************************************************************
** Piperka Client
** Copyright (C) 2019  Kari Pahula
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License along
** with this program; if not, write to the Free Software Foundation, Inc.,
** 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
**************************************************************************/

import QtQuick 2.0
import Sailfish.Silica 1.0
import "../components"

Page {
    id: updatesPage
    allowedOrientations: Orientation.All

    BusyIndicator {
        size: BusyIndicatorSize.Large
        anchors.centerIn: parent
        running: user.loading
    }

    Component.onCompleted: {
        updatesModel.sortType = 0;
    }

    SilicaListView {
        id: updatesList
        model: updatesModel
        anchors.fill: parent
        currentIndex: -1

        VerticalScrollDecorator {
            flickable: updatesList
        }

        PullDownMenu {
            MenuItem {
                id: syncNow
                text: qsTr("Synchronize now")
                onClicked: user.syncNow(true)
            }
        }

        header: Column {
            width: parent.width

            PageHeader {
                id: header
                title: qsTr("Updates")
            }

            TextSwitch {
                id: offsetBack
                text: qsTr("Offset back by one")
                checked: user.offsetBack
                anchors {
                    leftMargin: Theme.horizontalPageMargin
                    rightMargin: Theme.horizontalPageMargin
                }
                onClicked: user.offsetBack = offsetBack.checked
            }

            ComboBox {
                id: sortType
                label: qsTr("Sort type")
                currentIndex: updatesModel.sortType
                menu: ContextMenu {
                    MenuItem {
                        text: qsTr("Least new pages first")
                    }
                    MenuItem {
                        text: qsTr("Most recently updated")
                    }
                    MenuItem {
                        text: qsTr("Alphabetical")
                    }
                    onClicked: {
                        updatesModel.sortType = sortType.currentIndex
                    }
                }
            }
        }

        delegate: ListItem {
            id: delegate

            Label {
                id: unreadCount
                x: Theme.horizontalPageMargin
                width: Theme.itemSizeSmall
                anchors.verticalCenter: parent.verticalCenter
                text: unread_count
            }

            Label {
                text: title
                anchors {
                    left: unreadCount.right
                    verticalCenter: parent.verticalCenter
                }
            }

            NsfwMarker { visible: nsfw }

            onClicked: {
                pageModel.loadComic(cid);
                pageModel.autoBookmark = true;
                pageModel.autoSwitch = true;
                pageStack.push(Qt.resolvedUrl("ReaderPage.qml"))
            }
        }
    }
}
