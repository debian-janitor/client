/**************************************************************************
** Piperka Client
** Copyright (C) 2019  Kari Pahula
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License along
** with this program; if not, write to the Free Software Foundation, Inc.,
** 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
**************************************************************************/

import QtQuick 2.0
import Sailfish.Silica 1.0

Dialog {
    id: dialog
    allowedOrientations: Orientation.All

    Column {
        id: column
        width: parent.width

        DialogHeader {
            title: qsTr("Login")
        }

        TextField {
            id: nameField
            width: parent.width
            inputMethodHints: Qt.ImhNoAutoUppercase
            label: qsTr("User name")
            placeholderText: qsTr("User name")
            text: user.storedLoginName
            EnterKey.enabled: text.length >= 2
            EnterKey.iconSource: "image://theme/icon-m-enter-next"
            EnterKey.onClicked: passwordField.focus = true
        }

        PasswordField {
            id: passwordField
            width: parent.width
            label: qsTr("Password")
            placeholderText: qsTr("Password")
            EnterKey.enabled: text.length >= 2
            EnterKey.onClicked: dialog.accept()
        }

        TextSwitch {
            id: rememberField
            width: parent.width
            text: qsTr("Remember me")
            checked: user.rememberMe
        }

        TextSwitch {
            id: importBookmarks
            width: parent.width
            text: qsTr("Import bookmarks")
            description: qsTr("Importing bookmarks will overwrite the ones on server. Not importing will discard local bookmarks.")
            visible: user.localBookmarks()
        }
    }

    onAccepted: {
        user.login(nameField.text, passwordField.text, rememberField.checked, importBookmarks.checked)
    }
}
