/**************************************************************************
** Piperka Client
** Copyright (C) 2019  Kari Pahula
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License along
** with this program; if not, write to the Free Software Foundation, Inc.,
** 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
**************************************************************************/

import QtQuick 2.9
import QtQuick.Controls 2.4
import "qrc:/qml"

ListView {
    width: appWindow.width
    height: appWindow.height
    headerPositioning: ListView.PullBackHeader
    ScrollBar.vertical: ScrollBar {}

    Component.onCompleted: {
        browseModel.filterSubscribed = false;
        browseModel.sortType = 0;
        browseModel.setSearch("")
        pageModel.autoBookmark = false;
        pageModel.autoSwitch = false;
        if (!user.browseHelpSeen) {
            helpPopup.open()
        }
    }

    BusyIndicator {
        anchors.centerIn: parent
        running: user.loading
    }

    header: Rectangle {
        id: myHeader
        width: parent.width
        height: headerColumn.height
        z: 2

        Column {
            id: headerColumn
            width: parent.width
            Row {
                spacing: 2
                Button {
                    text: "<"
                    onClicked: {
                        pageStack.pop()
                    }
                    visible: platform.explicitBackButtons
                }

                Button {
                    text: qsTr("Options")
                    onClicked: {
                        optionsPopup.open()
                    }
                }
            }

            Item {
                height: searchInput.height
                width: parent.width

                Label {
                    id: searchLabel
                    anchors {
                        left: parent.left
                        rightMargin: 4
                        verticalCenter: parent.verticalCenter
                    }
                    text: qsTr("Search")
                }
                TextField {
                    id: searchInput
                    anchors {
                        left: searchLabel.right
                        right: parent.right
                    }
                    onEditingFinished: browseModel.setSearch(text)
                }
            }
        }

        Label {
            anchors.right: parent.right
            text: qsTr("Browse comics")
        }
    }
    model: browseModel
    delegate: BrowseItem {
        bookmarkFirst: bookmarkFirstSwitch.checked
    }

    Popup {
        id: optionsPopup
        width: appWindow.width*5/6
        x: appWindow.width/6
        modal: true
        focus: true
        Column {
            width: parent.width
            Text {
                text: qsTr("Options")
            }

            ComboBox {
                id: sortType
                currentIndex: 0
                width: parent.width
                model: ListModel {
                    ListElement {
                        text: qsTr("Alphabetical")
                    }
                    ListElement {
                        text: qsTr("Most popular")
                    }
                    ListElement {
                        text: qsTr("Date added")
                    }
                    ListElement {
                        text: qsTr("Most recently updated")
                    }
                }
                onCurrentIndexChanged: {
                    browseModel.sortType = sortType.currentIndex
                }
            }

            CheckBox {
                id: bookmarkFirstSwitch
                text: qsTr("Bookmark first page")
                checked: true
            }

            CheckBox {
                id: showOnlySubscribed
                objectName: "browseSubscribed"
                text: qsTr("Show only subscribed comics")
                onClicked: browseModel.filterSubscribed = checked
            }
        }
    }

    Popup {
        id: helpPopup
        width: appWindow.width*2/3
        x: appWindow.width/6
        modal: true
        focus: true

        Text {
            text: qsTr("Press and hold a comic to see more options.")
            width: parent.width
            wrapMode: Text.WordWrap
        }

        onClosed: user.browseHelpSeen = true
    }
}
