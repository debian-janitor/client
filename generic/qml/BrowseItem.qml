/**************************************************************************
** Piperka Client
** Copyright (C) 2019  Kari Pahula
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License along
** with this program; if not, write to the Free Software Foundation, Inc.,
** 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
**************************************************************************/

import QtQuick 2.9
import QtQuick.Controls 2.4

MouseArea {
    id: delegate
    property bool bookmarkFirst: true

    width: appWindow.width
    height: subscribeStar.height*2

    Label {
        id: subscribeStar
        visible: subscribed
        text: "★"
        anchors.verticalCenter: parent.verticalCenter
    }

    Label {
        x: subscribeStar.width
        text: title
        anchors.verticalCenter: parent.verticalCenter
        style: contextMenu.opened ? Text.Sunken : Text.Normal
    }
    onPressAndHold: {
        contextMenu.popup()
    }
    onClicked: {
        pageModel.loadComic(cid)
        pageStack.push(Qt.resolvedUrl("ReaderPage.qml"))
    }

    NsfwMarker { visible: nsfw }

    Menu {
        id: contextMenu
        width: onPiperka.width
        modal: true

        MenuItem {
            text: qsTr("Subscribe")
            visible: !subscribed
            height: visible ? implicitHeight : 0
            onClicked: user.subscribe(cid, bookmarkFirst)
        }

        MenuItem {
            text: qsTr("Unsubscribe")
            visible: subscribed
            height: visible ? implicitHeight : 0
            onClicked: user.unsubscribe(cid)
        }

        MenuItem {
            id: onPiperka
            text: qsTr("View entry on Piperka")
            onClicked: Qt.openUrlExternally("https://piperka.net/info.html?cid="+cid)
        }
    }
}
