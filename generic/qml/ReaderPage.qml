/**************************************************************************
** Piperka Client
** Copyright (C) 2019  Kari Pahula
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License along
** with this program; if not, write to the Free Software Foundation, Inc.,
** 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
**************************************************************************/

import QtQuick 2.9
import QtQuick.Controls 2.4
import QtWebView 1.1

Item {
    property bool makeFullScreen: true
    id: readerPage
    width: appWindow.width
    height: appWindow.height

    BusyIndicator {
        anchors.centerIn: parent
        running: user.loading
    }

    WebView {
        id: webView

        anchors {
            top: parent.top
            left: parent.left
            right: parent.right
            bottom: naviRow.top
        }
        url: pageModel.cursorUri

        visible: !pageModel.allRead
    }

    Item {
        id: naviRow
        anchors {
            left: parent.left
            right: parent.right
            bottom: parent.bottom
        }
        height: prev.height

        Button {
            anchors.left: parent.left
            id: prev
            text: "⇐"
            enabled: !pageModel.allRead && pageModel.cursor.row > 0
            onClicked: pageModel.setCursor(pageModel.cursor.row-1)
        }

        Button {
            anchors.left: prev.right
            id: back
            text: "<"
            onClicked: pageStack.pop()
            visible: platform.explicitBackButtons
        }

        Button {
            anchors {
                left: platform.explicitBackButtons ? back.right : prev.right
                right: next.left
            }
            id: options
            text: (1+pageModel.cursor.row) + "/" + (pageModel.rowCount-1)
            enabled: !pageModel.allRead
            onClicked: pageStack.push(Qt.resolvedUrl("PageDetailPage.qml"))
        }

        Button {
            anchors.right: parent.right
            id: next
            text: pageModel.nextIsSwitch ? "↵" : "⇒"
            enabled: pageModel.haveNext
            onClicked: {
                if (pageModel.nextIsSwitch) {
                    if (!pageModel.switchNext()) {
                        var depth = pageStack.depth
                        var i = 0;
                        while (i++ < depth) {
                            pageStack.pop(StackView.Immediate)
                        }
                        pageStack.push(Qt.resolvedUrl("AllReadPage.qml"), StackView.Immediate);
                    }
                } else
                    pageModel.setCursorNext()
            }
        }
    }
}
