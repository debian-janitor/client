/**************************************************************************
** Piperka Client
** Copyright (C) 2019  Kari Pahula
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License along
** with this program; if not, write to the Free Software Foundation, Inc.,
** 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
**************************************************************************/

import QtQuick 2.9
import QtQuick.Controls 2.4
import "qrc:/qml"

Item {
    id: recommendPage
    width: appWindow.width
    height: appWindow.height

    Component.onCompleted: {
        recommendModel.load();
    }

    Column {
        anchors.centerIn: parent
        width: parent.width
        visible: !user.loading && recommendModel.noRecommendations

        Label {
            width: parent.width
            wrapMode: Text.WordWrap
            text: qsTr("For some reason, no recommendations were received. Please try again later.")
        }
    }

    ListView {
        id: recommendList
        model: recommendModel
        anchors.fill: parent
        currentIndex: -1
        ScrollBar.vertical: ScrollBar {}

        header: Item {
            height: back.visible ? back.height : label.height

            Button {
                id: back
                text: "<"
                onClicked: pageStack.pop()
                visible: platform.explicitBackButtons
            }

            Label {
                id: label
                text: qsTr("Recommendations")
                anchors {
                    top: parent.top
                    right: parent.right
                }
            }
        }

        delegate: BrowseItem { }
    }
}
