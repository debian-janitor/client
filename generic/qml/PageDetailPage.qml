/**************************************************************************
** Piperka Client
** Copyright (C) 2019  Kari Pahula
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License along
** with this program; if not, write to the Free Software Foundation, Inc.,
** 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
**************************************************************************/

import QtQuick 2.9
import QtQuick.Controls 2.4

ListView {
    property bool makeFullScreen: true
    width: appWindow.width
    height: appWindow.height
    headerPositioning: ListView.PullBackHeader
    ScrollBar.vertical: ScrollBar {}

    id: pageList
    model: pageModel
    currentIndex: -1

    header: Rectangle {
        id: myHeader
        width: parent.width
        height: headerColumn.height
        z: 2

        Label {
            anchors {
                right: parent.right
                top: parent.top
            }
            text: qsTr("Details")
        }

        Column {
            id: headerColumn
            width: parent.width

            Button {
                id: back
                text: "<"
                onClicked: pageStack.pop()
                visible: platform.explicitBackButtons
            }

            Item {
                width: 1
                height: 5
            }

            Label {
                text: qsTr("Page URL")+":\n" + pageModel.cursorUri
                width: parent.width
                wrapMode: Text.WrapAnywhere
            }

            Item {
                width: 1
                height: 5
            }

            Row {
                width: parent.width
                spacing: 5

                Button {
                    id: inBrowser
                    text: qsTr("Open in browser")
                    onClicked: Qt.openUrlExternally(pageModel.cursorUri)
                }

                Button {
                    text: qsTr("Homepage")
                    onClicked: Qt.openUrlExternally(pageModel.homepage)
                }
            }

            Label {
                text: qsTr("Jump to...")
                anchors.right: parent.right
            }

            Row {
                width: parent.width
                spacing: 5

                Button {
                    text: qsTr("Currently viewing")
                    onClicked: {
                        pageList.positionViewAtIndex(pageModel.cursor.row, ListView.Center)
                    }
                }

                Button {
                    text: qsTr("Subscribed")
                    enabled: pageModel.subscription.valid
                    onClicked: {
                        pageList.positionViewAtIndex(pageModel.subscription.row, ListView.Center)
                    }
                }
            }

            Item {
                width: 1
                height: 5
            }

            Label {
                text: qsTr("Subscription controls")
                anchors.right: parent.right
            }

            CheckBox {
                text: qsTr("Move bookmark on navigation")
                checked: pageModel.autoBookmark
                onClicked: pageModel.autoBookmark = checked
            }
        }
    }

    delegate: MouseArea {
        id: delegate
        width: parent.width
        height: 2 * ordText.height

        Row {
            Label {
                id: ordText
                width: 60
                text: currentMarker ? "" : (1+ord)
                font.bold: cursor
                anchors.verticalCenter: parent.verticalCenter
                style: contextMenu.opened ? Text.Sunken : Text.Normal
                visible: !currentMarker
            }

            Label {
                text: "★"
                visible: subscribed
                anchors.verticalCenter: parent.verticalCenter
            }

            Label {
                text: name !== undefined ? name : ""
                font.bold: cursor
                anchors.verticalCenter: parent.verticalCenter
                visible: !currentMarker && name !== undefined
            }

            Label {
                text: qsTr("Newest page")
                font.italic: true
                font.bold: cursor
                anchors.verticalCenter: parent.verticalCenter
                visible: name == undefined && !currentMarker
            }

            Label {
                text: qsTr("Current")
                font.italic: true
                anchors.verticalCenter: parent.verticalCenter
                visible: currentMarker
            }
        }

        onPressAndHold: {
            contextMenu.popup()
        }

        onClicked: {
            pageModel.setCursor(ord)
            pageStack.pop()
        }

        Menu {
            id: contextMenu
            width: menuSetBookmark.width
            modal: true

            MenuItem {
                id: menuSetBookmark
                text: qsTr("Set bookmark")
                onClicked: {
                    if (currentMarker)
                        user.subscribe(pageModel.cid(), false);
                    else
                        user.subscribeAt(pageModel.cid(), ord);
                }
            }
        }
    }
}
