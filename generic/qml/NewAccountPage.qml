/**************************************************************************
** Piperka Client
** Copyright (C) 2019  Kari Pahula
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License along
** with this program; if not, write to the Free Software Foundation, Inc.,
** 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
**************************************************************************/

import QtQuick 2.9
import QtQuick.Controls 2.4
import net.piperka 0.1

Item {
    id: dialog
    signal create()
    width: appWindow.width
    height: appWindow.height

    onCreate: {
        if (nameField.acceptableInput && passwordField.acceptableInput && passwordFieldAgain.acceptableInput) {
            user.createAccount(nameField.text, emailField.text,
                               passwordField.text, rememberField.checked)
            pageStack.pop()
        }
    }

    Label {
        anchors {
            right: parent.right
            top: parent.top
        }
        text: qsTr("Create account")
    }

    Column {
        width: parent.width
        spacing: 5

        Button {
            id: back
            text: "<"
            onClicked: pageStack.pop()
            visible: platform.explicitBackButtons
        }

        Label {
            text: qsTr("Account name reserved. Please try again.")
            visible: user.storedCreatePassword != ""
            wrapMode: Text.WordWrap
        }

        TextField {
            id: nameField
            width: parent.width
            inputMethodHints: Qt.ImhNoAutoUppercase
            placeholderText: qsTr("User name")
            validator: RegExpValidator { regExp: /^.{2,}/ }
            onAccepted: emailField.focus = true
        }

        TextField {
            id: emailField
            width: parent.width
            inputMethodHints: Qt.ImhNoAutoUppercase
            placeholderText: qsTr("email (optional)")
            text: user.storedCreateEmail
            onAccepted: passwordField.focus = true
        }

        TextField {
            id: passwordField
            width: parent.width
            placeholderText: qsTr("Password")
            validator: RegExpValidator { regExp: /^.{4,}/ }
            objectName: "newAccountPassword"
            text: user.storedCreatePassword
            onAccepted: passwordFieldAgain.focus = true
        }

        TextField {
            id: passwordFieldAgain
            width: parent.width
            placeholderText: qsTr("Retype password")
            validator: PasswordValidator { }
            text: user.storedCreatePassword
            onAccepted: dialog.create()
        }

        CheckBox {
            id: rememberField
            width: parent.width
            text: qsTr("Remember me")
            checked: user.rememberMe
        }

        Button {
            id: newAccountButton
            text: qsTr("Create account")
            enabled: nameField.acceptableInput && passwordField.acceptableInput && passwordFieldAgain.acceptableInput
            onClicked: dialog.create()
        }
    }
}
