/**************************************************************************
** Piperka Client
** Copyright (C) 2019  Kari Pahula
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License along
** with this program; if not, write to the Free Software Foundation, Inc.,
** 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
**************************************************************************/

import QtQuick 2.9
import QtQuick.Controls 2.4

Item {
    width: appWindow.width
    height: appWindow.height

    Button {
        id: back
        text: "<"
        onClicked: pageStack.pop()
        anchors {
            left: parent.left
            top: parent.top
        }
        visible: platform.explicitBackButtons
    }

    Label {
        anchors.centerIn: parent
        width: appWindow.width
        text: qsTr("The server didn't recognize your user session. You have been logged out. Please try logging in again.")
        wrapMode: Text.WordWrap
    }
}
