/**************************************************************************
** Piperka Client
** Copyright (C) 2019  Kari Pahula
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License along
** with this program; if not, write to the Free Software Foundation, Inc.,
** 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
**************************************************************************/

import QtQuick 2.9
import QtQuick.Controls 2.4

Item {
    id: networkErrorPage
    width: appWindow.width
    height: appWindow.height

    Item {
        width: parent.width
        anchors {
            left: parent.left
            top: parent.top
        }

        Button {
            text: "<"
            onClicked: pageStack.pop()
            visible: platform.explicitBackButtons
        }
    }

    Column {
        anchors.centerIn: parent
        width: parent.width

        Label {
            text: qsTr("Network failure")
            width: parent.width
            wrapMode: Text.WordWrap
        }

        Label {
            visible: user.networkErrorMessage != ""
            text: qsTr("Detail")+": " + user.networkErrorMessage
            width: parent.width
            wrapMode: Text.WrapAtWordBoundaryOrAnywhere
        }
    }
}
