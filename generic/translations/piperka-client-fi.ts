<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fi">
<context>
    <name>AllReadPage</name>
    <message>
        <location filename="../qml/AllReadPage.qml" line="42"/>
        <source>All caught up!</source>
        <translation>Kaikki luettu!</translation>
    </message>
</context>
<context>
    <name>BrowseItem</name>
    <message>
        <location filename="../qml/BrowseItem.qml" line="59"/>
        <source>Subscribe</source>
        <translation>Tilaa</translation>
    </message>
    <message>
        <location filename="../qml/BrowseItem.qml" line="66"/>
        <source>Unsubscribe</source>
        <translation>Poista tilaus</translation>
    </message>
    <message>
        <location filename="../qml/BrowseItem.qml" line="74"/>
        <source>View entry on Piperka</source>
        <translation>Katso sarjakuvan tiedot Piperkassa</translation>
    </message>
</context>
<context>
    <name>BrowsePage</name>
    <message>
        <location filename="../qml/BrowsePage.qml" line="64"/>
        <location filename="../qml/BrowsePage.qml" line="114"/>
        <source>Options</source>
        <translation>Asetukset</translation>
    </message>
    <message>
        <location filename="../qml/BrowsePage.qml" line="82"/>
        <source>Search</source>
        <translation>Etsi</translation>
    </message>
    <message>
        <location filename="../qml/BrowsePage.qml" line="97"/>
        <source>Browse comics</source>
        <translation>Selaa sarjakuvalistaa</translation>
    </message>
    <message>
        <location filename="../qml/BrowsePage.qml" line="123"/>
        <source>Alphabetical</source>
        <translation>Aakkosellinen</translation>
    </message>
    <message>
        <location filename="../qml/BrowsePage.qml" line="126"/>
        <source>Most popular</source>
        <translation>Suosituin ensin</translation>
    </message>
    <message>
        <location filename="../qml/BrowsePage.qml" line="129"/>
        <source>Date added</source>
        <translation>Viimeksi lisätty ensin</translation>
    </message>
    <message>
        <location filename="../qml/BrowsePage.qml" line="132"/>
        <source>Most recently updated</source>
        <translation>Viimeksi päivitetty</translation>
    </message>
    <message>
        <location filename="../qml/BrowsePage.qml" line="142"/>
        <source>Bookmark first page</source>
        <translation>Aseta kirjanmerkki ensimmäiselle sivulle</translation>
    </message>
    <message>
        <location filename="../qml/BrowsePage.qml" line="148"/>
        <source>Show only subscribed comics</source>
        <translation>Näytä vain tilatut sarjakuvat</translation>
    </message>
    <message>
        <location filename="../qml/BrowsePage.qml" line="162"/>
        <source>Press and hold a comic to see more options.</source>
        <translation>Paina sarjakuvan nimeä valintojen avaamiseksi.</translation>
    </message>
</context>
<context>
    <name>ForceLogout</name>
    <message>
        <location filename="../qml/ForceLogout.qml" line="27"/>
        <source>The server didn&apos;t recognize your user session. You have been logged out. Please try logging in again.</source>
        <translation>Palvelin ei tunnistanut käyttäjäsessiotasi. Sinut on kirjattu ulos.</translation>
    </message>
</context>
<context>
    <name>LoginPage</name>
    <message>
        <location filename="../qml/LoginPage.qml" line="50"/>
        <location filename="../qml/LoginPage.qml" line="95"/>
        <source>Login</source>
        <translation>Kirjaudu</translation>
    </message>
    <message>
        <location filename="../qml/LoginPage.qml" line="59"/>
        <source>User name</source>
        <translation>Käyttäjänimi</translation>
    </message>
    <message>
        <location filename="../qml/LoginPage.qml" line="69"/>
        <source>Password</source>
        <translation>Salasana</translation>
    </message>
    <message>
        <location filename="../qml/LoginPage.qml" line="76"/>
        <source>Remember me</source>
        <translation>Muista minut</translation>
    </message>
    <message>
        <location filename="../qml/LoginPage.qml" line="84"/>
        <source>Import bookmarks</source>
        <translation>Tuo kirjanmerkit</translation>
    </message>
    <message>
        <location filename="../qml/LoginPage.qml" line="90"/>
        <source>Importing bookmarks will overwrite the ones on server. Not importing will discard local bookmarks.</source>
        <translation>Kirjanmerkkien tuominen ylikirjoittaa palvelimelle tallennetut kirjanmerkit. Tuomatta jättäminen hylkää kirjanmerkit.</translation>
    </message>
</context>
<context>
    <name>MainPage</name>
    <message>
        <location filename="../qml/MainPage.qml" line="56"/>
        <source>The last scheduled sync failed. The client will retry hourly.</source>
        <translation>Viimeisin ajastettu päivitys epäonnistui. Ohjelma yrittää uudestaan tunneittain.</translation>
    </message>
    <message>
        <location filename="../qml/MainPage.qml" line="58"/>
        <source>Last %L1 scheduled syncs failed. The client will retry hourly.</source>
        <translation>Viimeiset %L1 ajastettua päivitystä epäonnistuivat. Ohjelma yrittää uudestaan tunneittain.</translation>
    </message>
    <message>
        <location filename="../qml/MainPage.qml" line="98"/>
        <source>Synchronize now</source>
        <translation>Synkronoi nyt</translation>
    </message>
    <message>
        <location filename="../qml/MainPage.qml" line="98"/>
        <source>Please wait</source>
        <translation>Odota hetki</translation>
    </message>
    <message>
        <location filename="../qml/MainPage.qml" line="107"/>
        <source>Login</source>
        <translation>Kirjaudu</translation>
    </message>
    <message>
        <location filename="../qml/MainPage.qml" line="113"/>
        <source>Create account</source>
        <translation>Luo käyttäjätunnus</translation>
    </message>
    <message>
        <location filename="../qml/MainPage.qml" line="119"/>
        <location filename="../qml/MainPage.qml" line="232"/>
        <source>Logout</source>
        <translation>Kirjaudu ulos</translation>
    </message>
    <message>
        <location filename="../qml/MainPage.qml" line="132"/>
        <source>Browse comics</source>
        <translation>Selaa sarjakuvalistaa</translation>
    </message>
    <message>
        <location filename="../qml/MainPage.qml" line="144"/>
        <source>Recommendations</source>
        <translation>Suositukset</translation>
    </message>
    <message>
        <location filename="../qml/MainPage.qml" line="160"/>
        <source>Updates</source>
        <translation>Päivitykset</translation>
    </message>
    <message>
        <location filename="../qml/MainPage.qml" line="174"/>
        <source>Quick read</source>
        <translation>Pikaluku</translation>
    </message>
    <message>
        <location filename="../qml/MainPage.qml" line="188"/>
        <source>Select comics to read from the browse comics page.</source>
        <translation>Valitse luettavia sarjakuvia sarjakuvalistasta.</translation>
    </message>
    <message>
        <location filename="../qml/MainPage.qml" line="189"/>
        <source>You have no unread comics. Wait for updates or subscribe to more comics.</source>
        <translation>Sinulla ei ole lukemattomia sarjakuvia. Odota päivityksiä tai lisää valintoihisi uusia sarjakuvia.</translation>
    </message>
    <message>
        <location filename="../qml/MainPage.qml" line="215"/>
        <source>Log in or create account and subscribe to at least 5 comics to get recommendations.</source>
        <translation>Suosituksia nähdäksesi kirjaudu tai luo tunnus ja tilaa vähintään 5 sarjakuvaa.</translation>
    </message>
    <message>
        <location filename="../qml/MainPage.qml" line="229"/>
        <source>Confirm logout</source>
        <translation>Vahvista uloskirjautuminen</translation>
    </message>
</context>
<context>
    <name>NetworkErrorPage</name>
    <message>
        <location filename="../qml/NetworkErrorPage.qml" line="33"/>
        <source>Network failure</source>
        <translation>Verkkovirhe</translation>
    </message>
    <message>
        <location filename="../qml/NetworkErrorPage.qml" line="40"/>
        <source>Detail</source>
        <translation>Yksityiskohdat</translation>
    </message>
</context>
<context>
    <name>NewAccountPage</name>
    <message>
        <location filename="../qml/NewAccountPage.qml" line="42"/>
        <location filename="../qml/NewAccountPage.qml" line="106"/>
        <source>Create account</source>
        <translation>Luo tunnus</translation>
    </message>
    <message>
        <location filename="../qml/NewAccountPage.qml" line="55"/>
        <source>Account name reserved. Please try again.</source>
        <translation>Käyttäjänimi on varattu. Yritä uudestaan.</translation>
    </message>
    <message>
        <location filename="../qml/NewAccountPage.qml" line="64"/>
        <source>User name</source>
        <translation>Käyttäjänimi</translation>
    </message>
    <message>
        <location filename="../qml/NewAccountPage.qml" line="73"/>
        <source>email (optional)</source>
        <translation>email (vapaaehtoinen)</translation>
    </message>
    <message>
        <location filename="../qml/NewAccountPage.qml" line="81"/>
        <source>Password</source>
        <translation>Salasana</translation>
    </message>
    <message>
        <location filename="../qml/NewAccountPage.qml" line="91"/>
        <source>Retype password</source>
        <translation>Salasana uudestaan</translation>
    </message>
    <message>
        <location filename="../qml/NewAccountPage.qml" line="100"/>
        <source>Remember me</source>
        <translation>Muista minut</translation>
    </message>
</context>
<context>
    <name>PageDetailPage</name>
    <message>
        <location filename="../qml/PageDetailPage.qml" line="44"/>
        <source>Details</source>
        <translation>Yksityiskohdat</translation>
    </message>
    <message>
        <source>Page URL: </source>
        <translation type="vanished">Sivun URL: </translation>
    </message>
    <message>
        <location filename="../qml/PageDetailPage.qml" line="63"/>
        <source>Page URL</source>
        <translation>Sivun URL</translation>
    </message>
    <message>
        <location filename="../qml/PageDetailPage.qml" line="79"/>
        <source>Open in browser</source>
        <translation>Avaa selaimessa</translation>
    </message>
    <message>
        <location filename="../qml/PageDetailPage.qml" line="84"/>
        <source>Homepage</source>
        <translation>Kotisivu</translation>
    </message>
    <message>
        <location filename="../qml/PageDetailPage.qml" line="90"/>
        <source>Jump to...</source>
        <translation>Hyppää...</translation>
    </message>
    <message>
        <location filename="../qml/PageDetailPage.qml" line="99"/>
        <source>Currently viewing</source>
        <translation>Nyt avattu sivu</translation>
    </message>
    <message>
        <location filename="../qml/PageDetailPage.qml" line="106"/>
        <source>Subscribed</source>
        <translation>Tilattu sivu</translation>
    </message>
    <message>
        <location filename="../qml/PageDetailPage.qml" line="120"/>
        <source>Subscription controls</source>
        <translation>Tilauksenhallinta</translation>
    </message>
    <message>
        <location filename="../qml/PageDetailPage.qml" line="125"/>
        <source>Move bookmark on navigation</source>
        <translation>Siirrä kirjanmerkkiä navigoidessa</translation>
    </message>
    <message>
        <location filename="../qml/PageDetailPage.qml" line="162"/>
        <source>Newest page</source>
        <translation>Uusin sivu</translation>
    </message>
    <message>
        <location filename="../qml/PageDetailPage.qml" line="170"/>
        <source>Current</source>
        <translation>Nykyinen</translation>
    </message>
    <message>
        <location filename="../qml/PageDetailPage.qml" line="193"/>
        <source>Set bookmark</source>
        <translation>Aseta kirjanmerkki</translation>
    </message>
</context>
<context>
    <name>RecommendPage</name>
    <message>
        <location filename="../qml/RecommendPage.qml" line="40"/>
        <source>For some reason, no recommendations were received. Please try again later.</source>
        <translation>Jostain syystä palvelimelta ei saatu suosituksia. Yritä uudestaan myöhemmin.</translation>
    </message>
    <message>
        <location filename="../qml/RecommendPage.qml" line="61"/>
        <source>Recommendations</source>
        <translation>Suositukset</translation>
    </message>
</context>
<context>
    <name>UpdatesPage</name>
    <message>
        <location filename="../qml/UpdatesPage.qml" line="44"/>
        <source>Updates</source>
        <translation>Päivittyneet sarjakuvat</translation>
    </message>
    <message>
        <source>offset back by one</source>
        <translation type="vanished">Avaa luettu sivu ensin</translation>
    </message>
    <message>
        <location filename="../qml/UpdatesPage.qml" line="59"/>
        <source>Offset back by one</source>
        <translation>Avaa luettu sivu ensin</translation>
    </message>
    <message>
        <location filename="../qml/UpdatesPage.qml" line="71"/>
        <source>Sort type</source>
        <translation>Lajittelutapa</translation>
    </message>
    <message>
        <location filename="../qml/UpdatesPage.qml" line="85"/>
        <source>Least new pages first</source>
        <translation>Vähiten päivittyneita sivuja ensin</translation>
    </message>
    <message>
        <location filename="../qml/UpdatesPage.qml" line="88"/>
        <source>Most recently updated</source>
        <translation>Viimeksi päivitetty</translation>
    </message>
    <message>
        <location filename="../qml/UpdatesPage.qml" line="91"/>
        <source>Alphabetical</source>
        <translation>Aakkosellinen</translation>
    </message>
</context>
</TS>
