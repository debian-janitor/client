TARGET = piperka-client

SOURCES += src/piperka-client.cpp \
    ../src/comic.cpp \
    ../src/download.cpp \
    ../src/user.cpp \
    ../src/subscription.cpp \
    ../src/updates.cpp \
    ../src/browse.cpp \
    ../src/application.cpp \
    ../src/page.cpp \
    ../src/sortmanager.cpp \
    ../src/passwordvalidator.cpp \
    ../src/recommend.cpp \
    src/platform.cpp

DISTFILES += \
    android/AndroidManifest.xml \
    android/build.gradle \
    android/res/values/libs.xml \
    qml/BrowsePage.qml \
    qml/BrowseItem.qml \
    qml/ReaderPage.qml \
    qml/AllReadPage.qml \
    qml/LoginPage.qml \
    qml/NewAccountPage.qml \
    qml/PageDetailPage.qml \
    qml/RecommendPage.qml \
    qml/UpdatesPage.qml \
    qml/MainPage.qml \
    qml/ForceLogout.qml \
    qml/NetworkErrorPage.qml \
    qml/NsfwMarker.qml

RESOURCES = piperka-client.qrc

TRANSLATIONS += \
  translations/piperka-client-fi.ts

HEADERS += \
    ../src/comic.h \
    ../src/download.h \
    ../src/user.h \
    ../src/subscription.h \
    ../src/updates.h \
    ../src/browse.h \
    ../src/application.h \
    ../src/page.h \
    ../src/sortmanager.h \
    ../src/passwordvalidator.h \
    ../src/recommend.h \
    src/platform.h

VERSION = $$system("grep -E '^\*' ../CHANGES | head -n 1 | sed -r 's/^.+ ([0-9.]+\.[0-9]+).*/\1/'")

DEFINES += APP_VERSION=\\\"$$VERSION\\\" CLIENT_NAME=\\\"GenericPiperka\\\"

QT += network qml quick webview

contains(ANDROID_TARGET_ARCH,armeabi-v7a) {
    ANDROID_PACKAGE_SOURCE_DIR = \
        $$PWD/android
}

contains(ANDROID_TARGET_ARCH,arm64-v8a) {
    ANDROID_PACKAGE_SOURCE_DIR = \
        $$PWD/android
}

contains(ANDROID_TARGET_ARCH,x86) {
    ANDROID_PACKAGE_SOURCE_DIR = \
        $$PWD/android
}
