/**************************************************************************
** Piperka Client
** Copyright (C) 2019  Kari Pahula
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License along
** with this program; if not, write to the Free Software Foundation, Inc.,
** 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
**************************************************************************/

#include <QtGui/QGuiApplication>
#include <QtQml/QQmlContext>
#include <QtQuick/QQuickView>
#include <QCommandLineParser>
#include <QQmlApplicationEngine>

#include "../src/application.h"
#include "platform.h"

int main(int argc, char *argv[])
{
    QCoreApplication::setOrganizationName("piperka.net");
    QCoreApplication::setApplicationName("piperka-client");
    QGuiApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QGuiApplication app(argc, argv);
    QCoreApplication::setApplicationName("piperka-client");
    QCoreApplication::setApplicationVersion(APP_VERSION);

    QCommandLineParser parser;
    parser.addHelpOption();
    parser.addVersionOption();
    parser.process(app);

    QQmlApplicationEngine engine;
    QQmlContext *ctxt = engine.rootContext();
    Platform platform;
    ctxt->setContextProperty("platform", &platform);

    Application application(ctxt);
    engine.addImportPath("qrc:///qml");
    engine.load(QUrl("qrc:qml/MainPage.qml"));
    application.viewComplete();

    app.exec();
}
