/**************************************************************************
** Piperka Client
** Copyright (C) 2019  Kari Pahula
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License along
** with this program; if not, write to the Free Software Foundation, Inc.,
** 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
**************************************************************************/

#include "subscription.h"

Subscription::Subscription(const QJsonArray &arr, QObject *parent)
    : QObject(parent)
    , m_cid(arr.at(0).toInt(-1))
    , m_ordOffset(arr.at(1).toInt(0))
    , m_maxord(arr.at(2).toInt(0))
    , m_maxsubord(arr.at(3).toInt(0))
    , m_num(arr.at(4).toInt(0))
{
}

bool Subscription::update(const QJsonArray &arr)
{
    int cid = arr.at(0).toInt(-1);
    if (cid != m_cid)
        return false;

    int old = m_ordOffset;
    m_cid = cid;
    m_ordOffset = arr.at(1).toInt();
    m_maxord = arr.at(2).toInt();
    m_maxsubord = arr.at(3).toInt();
    m_num = arr.at(4).toInt();
    if (m_ordOffset != old)
        emit move(old);
    return true;
}

std::ostream &operator<<(std::ostream &os, const Subscription &sub)
{
    return os << "subscription " << sub.cid()
        << " " << sub.ord()
        << " " << sub.max_ord()
        << " " << sub.max_sub_ord()
        << " " << sub.num();
}

QDebug operator<<(QDebug d, const Subscription &sub)
{
    return d << "subscription" << sub.cid()
        << sub.ord()
        << sub.max_ord()
        << sub.max_sub_ord()
        << sub.num();
}
