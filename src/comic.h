/**************************************************************************
** Piperka Client
** Copyright (C) 2019  Kari Pahula
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License along
** with this program; if not, write to the Free Software Foundation, Inc.,
** 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
**************************************************************************/

#pragma once

#include <QAbstractListModel>
#include <QJsonDocument>
#include <QPointer>

#include "subscription.h"

class Comic
{
public:
    Comic(const int &native, const int &cid, const QString &title, const bool &nsfw) : mNative(native), mCid(cid), mTitle(title), mNSFW(nsfw) {}

    int nativeOrder() const {return mNative;}
    int cid() const {return mCid;}
    QString title() const {return mTitle;}
    bool nsfw() const { return mNSFW; }
    bool subscribed() const { return !mSubscription.isNull(); }
    QPointer<Subscription> subscription() const { return mSubscription; }

    void setSubscription(const QPointer<Subscription> subscription);
private:
    int mNative;
    int mCid;
    QString mTitle;
    bool mNSFW;
    QPointer<Subscription> mSubscription;
};

class ComicModel : public QAbstractListModel
{
    Q_OBJECT
public:
    enum ComicRoles {
        CidRole = Qt::UserRole + 1
        ,TitleRole
        ,NSFWRole
        ,NativeOrderRole
        ,SubscribedRole
        ,UnreadCountRole
    };

    ComicModel(QObject *parent = 0);

    void setSubscription(const QPointer<Subscription> &subscription);

    int rowCount(const QModelIndex &parent = QModelIndex()) const;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;

signals:
     void downloadComicsListPrepare();

public slots:
     void downloadComicsListComplete(const QJsonDocument &doc);
     void downloadComicsListUnchanged();
     void newSubscription(const QPointer<Subscription> &subscription);
     void refreshSubscription(const QPointer<Subscription> &subscription);
     void pendingSubscriptionsComplete();

private slots:
    void subscriptionDeleted();

protected:
    QHash<int, QByteArray> roleNames() const;
private:
    void finishComicsListSync();

    // values offset by +1
    QHash<int,int> cid_row;
    QList<Comic> m_comics;
    bool loadPending = false;
    bool syncPending = false;
    QMap<int,QPointer<Subscription>> pendingSubscriptions;
    QList<QPointer<Subscription>> pendingSubscriptionRefresh;
    QVector<Comic *> toAdd;
};
