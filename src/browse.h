/**************************************************************************
** Piperka Client
** Copyright (C) 2019  Kari Pahula
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License along
** with this program; if not, write to the Free Software Foundation, Inc.,
** 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
**************************************************************************/

#pragma once

#include <QSortFilterProxyModel>

#include "comic.h"
#include "sortmanager.h"

/* ComicModel with filters/sorting for BrowsePage */
class BrowseModel : public QSortFilterProxyModel
{
    Q_OBJECT
    Q_PROPERTY(bool filterSubscribed READ filterSubscribed WRITE setFilterSubscribed NOTIFY filterSubscribedChanged)
    Q_PROPERTY(int sortType READ sortType WRITE setSortType NOTIFY sortTypeChanged)
public:
    BrowseModel(const SortManager &mgr, QObject *parent = 0);

    bool filterSubscribed() const { return m_filterSubscribed; }
    void setFilterSubscribed(bool filter);

    int sortType() const { return m_sortType; }
    void setSortType(int sortType);

    Q_INVOKABLE void setSearch(const QString &search);

    void setSource(QAbstractItemModel *model);

signals:
    void filterSubscribedChanged();
    void sortTypeChangePrepare(int sortType);
    void sortTypeChanged();

public slots:
    void sortDownloaded(const int &sortType, const QJsonDocument &doc);

protected:
    bool filterAcceptsRow(int sourceRow, const QModelIndex &sourceParent) const override;
    bool filterAcceptsColumn(int sourceColumn, const QModelIndex &sourceParent) const override {
        Q_UNUSED(sourceColumn);
        Q_UNUSED(sourceParent);
        return true;
    }
    bool lessThan(const QModelIndex &source_left, const QModelIndex &source_right) const override;

private:
    bool m_filterSubscribed = false;
    int m_sortType = 0;
    const SortManager &m_mgr;
    QString m_search;
};
