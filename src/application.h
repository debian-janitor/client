/**************************************************************************
** Piperka Client
** Copyright (C) 2019  Kari Pahula
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License along
** with this program; if not, write to the Free Software Foundation, Inc.,
** 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
**************************************************************************/

#pragma once

#include <QObject>

#include "browse.h"
#include "comic.h"
#include "download.h"
#include "page.h"
#include "recommend.h"
#include "sortmanager.h"
#include "updates.h"
#include "user.h"

class Application : public QObject
{
    Q_OBJECT
public:
    explicit Application(QQmlContext *ctxt, QObject *parent = nullptr);

    void viewComplete();

signals:

public slots:
private:
    Download m_download;
    User m_user;
    ComicModel m_comics;
    BrowseModel m_browse;
    UpdatesModel m_updates;
    RecommendModel m_recommend;
    // TODO: Destroy PageModel when not needed and don't use it as a singleton
    PageModel m_page;

    SortManager m_sortManager;
};
