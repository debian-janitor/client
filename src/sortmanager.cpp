/**************************************************************************
** Piperka Client
** Copyright (C) 2019  Kari Pahula
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License along
** with this program; if not, write to the Free Software Foundation, Inc.,
** 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
**************************************************************************/

#include <QJsonArray>
#include <QJsonDocument>

#include "sortmanager.h"

SortManager::SortManager(QObject *parent) : QObject(parent)
{
}

void SortManager::sortDownloaded(const int &sortType, const QJsonDocument &doc)
{
    QJsonArray ordering = doc.array();
    QHash<int, int> &target = sortType == 1 ? mSortPopular : mSortUpdated;
    target.clear();
    int i = INT_MAX;
    for (QJsonArray::const_iterator iter = ordering.constBegin(); iter != ordering.constEnd(); --i, ++iter) {
        if (iter->isArray()) {
            QJsonArray sub = iter->toArray();
            for (QJsonArray::const_iterator iter2 = sub.constBegin(); iter2 != sub.constEnd(); ++iter2) {
                target.insert(iter2->toInt(), i);
            }
        } else {
            target.insert(iter->toInt(), i);
        }
    }
}
